# mm4b

Welcome to the map-matching package tailored for bicycles! The paper can be found here: https://doi.org/10.1049/itr2.12567.

## Preparatory work
1. Create a folder graph/raw/ to store raw road network information
2. Create mapMatch_result/ to store the processed road network
3. Create mapMatch_result/ptsDf_/ to store the selected road candidates (within 50m)
4. Create mapMatch_result/rlt_/viterbi/ to store the map-matched result
5. Put the OpenStreetMap shapefile in the raw_map folder
6. Put the raw GPS data as data/stepII.h5, csv form as data/stepII.csv. Columns:tripID;timestamp;lat;lon.

## Run steps
1. In local computer, run step1_simplify_graph.py and step2_main_road_evaluation.py 
2. Sbatch step3_supercomputer_run.sh on supercomputer or run parallel_run.py with function_name=getPts in server, an example command for server: python parallel_run.py --function_name getPts --input_file data/stepII.csv --outputdir mapMatch_result/ptsDf_/0.h5
3. Sbatch step4_supercomputer_run.sh on supercomputer or run parallel_run.py with function_name=matchTrace in server, an example command for server: python parallel_run.py --function_name matchTrace --outputdir mapMatch_result/rlt_/ --input_file mapMatch_result/ptsDf_/0.h5

## Output format
Output results are in mapMatch_result/rlt_/viterbi/, format: 
1. tripID: identity of each trip trajectory to be mapped
2. nid: recorded order of GPS point
3. edge: mapped road segment ID in the compact graph (section 3.2)
4. full_edge: 	mapped road segment ID in the raw graph
5. dist: distance (m) between GPS point and the mapped road 
6. frcalong: travelled ratio over this edge	
7. secs: time difference (s) from 2020-10-01	
8. seglength: 	travelled distance (km) over this edge

## Acknowledgement
Part of code in this repository is derived from "https://github.com/amillb/pgMapMatch".