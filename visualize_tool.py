import sys
sys.path.append('../')
from visual_map_matching import get_background
from utils import read_h5, get_accurate_start_end_point, colorFader
import argparse
import glob
import os
import datetime
from tqdm import tqdm
import pandas as pd
import numpy as np
import plotly.graph_objects as go

def find_trip_in_auto(tripID, inputfolder="../mapMatch_result/rlt_pg/"):
    for h5_file in glob.glob(inputfolder+"viterbi/*.h5"):
        all_df = read_h5(h5_file, time_converse=False)
        if not tripID in all_df['tripID'].values:
            continue 
        all_df["speed_"]=all_df['speed_'].round(3)
        all_df["timestamp"] = pd.to_timedelta(all_df.secs, unit='s')+datetime.datetime(2020, 10, 1)
        all_df.drop(["secs"], axis=1, inplace=True)   
        nb = h5_file.split("/")[-1].split(".h5")[0]
        txt_file=inputfolder+str(nb)+".txt"
        with open(txt_file, 'r') as file:
            lines = file.readlines()
            for line in lines:
                if str(tripID) == line.split(",")[0]:
                    break
        return all_df[all_df['tripID']==tripID], [int(i) for i in line.split(",")[1].split(" ")]

def plot_elements(df, edges, streetmap, edgesDf, tripID, raw_df, coarse2full_edge):
    Traces, Routes = [[], [], [], []], [[], [], None, []]
    _, _, projected_lon, projected_lat = get_accurate_start_end_point(df, streetmap, edgesDf)
    Projection = [projected_lon, projected_lat]


    raw_df = raw_df[raw_df['tripID']==tripID]
    Traces[0], Traces[1] = Traces[0]+raw_df.lon.values.tolist()+[None], Traces[1]+raw_df.lat.values.tolist()+[None]

    for index in range(len(raw_df)):
        if index in df.index.unique():
            row = df.loc[index]
            Traces[3]=Traces[3]+['tripID: '+str(row["tripID"])+
                "<br>Timestamps: "+str(row["timestamp"])+
                "<br>Coarse edge: "+str(row["edge"])+
                "<br>Uturn: "+str(row["uturn"])+
                "<br>Speed: "+str(row["speed_"])+
                "<br>Direction: "+str(row["dir"])+
                '<br>Fraction: '+str(row["frcalong"])]
        else:
            row = raw_df.iloc[index]
            Traces[3]=Traces[3]+['tripID: '+str(row["tripID"])+
                "<br>Timestamps: "+str(row["timestamp"])]
    Traces[3]=Traces[3]+[' ']
    raw_df = raw_df.copy()
    raw_df.loc[:,"timestamp"] = (raw_df.loc[:,"timestamp"]-datetime.datetime(2020, 10, 1)).dt.total_seconds()
    Traces[2]= Traces[2]+colorFader(raw_df.timestamp, c2='#FDC9C9', c1='#920808')  + ["#000000"]
    
    unique_full_edges = np.concatenate([coarse2full_edge[i] for i in edges])
    selected_edge=streetmap.loc[unique_full_edges]
    geo_list = []
    for index, row in selected_edge.iterrows():
        for j in row.geometry.coords:
            geo_list += [list(j)]
        Routes[3] = Routes[3] + ['Road type:'+row["type"]+
                        '<br>tripID: '+str(tripID)+
                        '<br>Coarse edge: '+str(row["c_edge"]) for i in range(len(row.geometry.coords))]+[' ']
        geo_list += [[None, None]]
    geo_list = np.asarray(geo_list)
    Routes[0], Routes[1] = geo_list[:,0], geo_list[:,1]
    return Traces, Routes, Projection

def plot_html(traces, routes, projection, background, tripID, outputfolder,suffix="", 
              line_marker_size=20, marker_size=10,):
    zoom_center = {"lat": 51.9248025, "lon":4.5}
    fig = go.Figure()
    # add background
    lons, lats, color, info = background
    line_trace0 = go.Scattermapbox(      
                mode = "markers+lines+text",
                lon = lons, lat = lats,
                marker = {'size': line_marker_size, 'color': '#CCFFFF'},line={'width':10, 'color':'#CCFFFF'},
                name = "Background",
                text=info)
    fig.add_trace(line_trace0)

    # add traces
    lons, lats, color, info = traces
    line_trace1 = go.Scattermapbox(      
                mode = "markers+lines+text",
                lon = lons, lat = lats,
                marker = {'size': line_marker_size, 'color': color},line={'width':5, 'color':'#FFCCE5'},
                name = "Trace",
                text=info)
    fig.add_trace(line_trace1)

    # add routes
    lons, lats, color, info = routes
    if color is None:
        color, edge_color="#5CA961", "#5CA961"
    else:  
        edge_color="#CCFFFF"
    line_trace2 = go.Scattermapbox(      
                mode = "markers+lines+text",
                lon = lons, lat = lats,
                marker = {'size': line_marker_size, 'color': color},line={'width':10, 'color':edge_color},
                name = "Selected_edge",
                text=info)
    fig.add_trace(line_trace2)

    # add projection
    lons, lats = projection
    point_trace = go.Scattermapbox(
                    mode = "markers",
                    lon = lons, lat = lats,
                    marker = {'size': marker_size, 'color': "#FF0000"},
                    name = str(tripID))
    fig.add_trace(point_trace) 

    fig.update_layout(mapbox_style="open-street-map")
    fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0}, mapbox = {
        'center': zoom_center,
        'zoom': 13})
    fig.update_layout(legend={"orientation":"h"})
    fig.update_layout(height=800, width=2500)
    fig.for_each_trace(lambda trace: trace.update(visible="legendonly"))
    fig.write_html(outputfolder+str(tripID)+suffix+".html")  
    
if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Visualize trips')
    parser.add_argument('--trip', default="", type=str, help='Trip to visualize')
    args = parser.parse_args()
    raw_df = pd.read_csv("selected_stepII.csv")
    raw_df = read_h5("../data/stepII.h5", True).sort_values(by=["tripID", "timestamp"])
    raw_df.timestamp = pd.to_datetime(raw_df.timestamp) #, format="%m/%d/%y %H:%M:%S")
    outputfolder = "trips_visualization/"

    if args.trip == "":
        all_tripIDs = raw_df.tripID.unique().tolist()
    else:
        all_tripIDs = [int(args.trip)]

    os.system("rm -r "+outputfolder)
    os.mkdir(outputfolder)

    background, coarse2full_edge, full2coarse_edge, streetmap, edgesDf = get_background("../mapMatch_result/full_roads.shp", "../mapMatch_result/coarse_roads.csv")
    # for tripID in tqdm(all_tripIDs):
    
    # for tripID in [int(i) for i in "54259 91200 8409 38839 28415 96376 83565 98376 18947 41151".split(" ")]:
    for tripID in pd.read_csv("selected_stepII.csv").tripID.unique():
    # for tripID in [4627, 13205, 16469, 18992, 30041, 31319, 36726, 45671, 74128, 80909, 89819]:
        mapped_trip_df, mapped_roads = find_trip_in_auto(tripID, "../mapMatch_result/rlt_test/")
        Traces, Routes, Projection = plot_elements(mapped_trip_df, mapped_roads, streetmap, edgesDf, tripID, raw_df, coarse2full_edge)
        plot_html(Traces, Routes, Projection, background, tripID, outputfolder, suffix="_auto")

        # mapped_trip_df, mapped_roads = find_trip_in_auto(tripID, "../mapMatch_result/rlt_comp_test/")
        # Traces, Routes, Projection = plot_elements(mapped_trip_df, mapped_roads, streetmap, edgesDf, tripID, raw_df, coarse2full_edge)
        # plot_html(Traces, Routes, Projection, background, tripID, outputfolder, suffix="_comp")

        # mapped_trip_df, mapped_roads = find_trip_in_auto(tripID, "../mapMatch_result/rlt_comp_test/")
        # Traces, Routes, Projection = plot_elements(mapped_trip_df, mapped_roads, streetmap, edgesDf, tripID, raw_df, coarse2full_edge)
        # plot_html(Traces, Routes, Projection, background, tripID, outputfolder, suffix="_comp")


    


