#!/bin/sh
#
#SBATCH --job-name=auto
#SBATCH --output=auto.log
#SBATCH --partition=compute
#SBATCH --time=10:00:00
#SBATCH --ntasks=59
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1GB
#SBATCH --account=research-ceg-tp
#SBATCH --exclusive


# Load modules:
module load miniconda3
module load 2022r2
module load intel/oneapi-all
export I_MPI_PMI_LIBRARY=/cm/shared/apps/slurm/current/lib64/libpmi2.so

# Set conda env:
unset CONDA_SHLVL
source "$(conda info --base)/etc/profile.d/conda.sh"

# Activate conda, run job, deactivate conda
# Change the name of the conda environment to the one you created
conda activate network

srun python delftblue_parallel.py DelftBlue_getsPts data/stepII.csv mapMatch_result/ptsDf_/