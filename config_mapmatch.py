# how many nodes pgrouting can route at once.
# If you have lots of memory, increase. If you get a memory error, reduce
maxNodes = 250

# All these parameters assume meters, but must match the units of your projection
# State Plane meters will normally work best
weight_1stlast = 1      # for the first and last node, how much distance weight is increased by. This helps to ensure the match is closer to and from the start and endpoint
gpsError = 50           # max distance that streets can be from GPS point to be considered
gpsError_fway = 70      # same, but for freeway (kmh>100) - because these roads are much wider
maxSpeed = 120           # speed threshold for deleting pings (kmh) in traceCleaner

# primary_punish = 1000
# secondary_punish = 0.5
# secondary_link_punish = 0.5
# tertiary_punish = 0.5



dist_threshold = 20
 
sigma_z = 4#10.0          # std dev parameter for geometric likelihood. Relates to GPS noise. Newson and Kummel 2009 say 4.0
sigma_t = 4 #0.3           # std dev parameter for temporal likelihood
sigma_topol = 0.6 #0.6      # std dev parameter for topological likelihood
temporal_scale = 1 #0.55   # scale parameter for temporal likelihood

temporal_weight = 1   # how more more the temporal likelihood is weighted relative to the distance likelihood score
topol_weight = 1     # how more more the topological likelihood is weighted relative to the distance likelihood score
skip_penalty = 5     # penalty for skipping a point is temporalLL(skip_penalty)
max_skip = 2       # maximum number of points to skip. Reducing this will improve performance
# uturnCost = 10        # if None, use the default (the average of the median cost and reverse cost in the edges)
allowFinalUturn = True  # if True, allow a U-turn on the final edge

# column identifiers for the PostGIS table of streets
# the default values here are compatible with osm2po
streetIdCol = 'id'          # unique id for street edge (i.e. segment or block)
streetGeomCol = 'geom_way'  # geometry column (LineString) for street edge
startNodeCol = 'source'     # id of node at which the street edge starts
endNodeCol = 'target'       # id of node at which the street edge ends
travelCostCol = 'cost'      # generalized cost to go from startNode to endNode
travelCostReverseCol = 'reverse_cost'  # generalized cost to go from endNode to startNode. Can be same as travelCostCol if you have no one-way streets
streetLengthCol = 'km'      # length of street, in km
speedLimitCol = 'kmh'       # speed limit on street, in km per hour

# SQL-compliant query that identifies freeways (with the higher gps error tolerance)
fwayQuery = 'clazz<15 OR kmh>=100'
# comma-separated list of columns that are needed in fwayQuery, but are not listed above
fwayCols = 'clazz'

### Input files and save paths
raw_osm_file = "raw_map/rotterdam_citycenter"
point_save_path = "graph/raw/" #intermediate output storing point and linestring dictionary
complete_graph_path = "mapMatch_result/full_roads.shp"
map_nx_path = "raw_map/graph.graphml"
simplified_graph_path = "mapMatch_result/coarse_roads.csv"
confirmed_coarse_path = "mapMatch_result/penalized_coarse_roads.csv" #file path storing all roads including confirmed main car roads
confirmed_roads_txt = "mapMatch_result/penalty_roads.txt"
confirmed_roads_html = "visualization/map_match/penalty_roads.html"


### Define coefficients for cost function
revser_coeffi = 0.2
alpha_coefficient = 1
beta_coefficient = 1
extra_mapped_penalty =5*alpha_coefficient # penalty for main car roads with low availability
alpha0, alpha1, alpha2, alpha3, alpha4 = 0, round(1*alpha_coefficient, 1), round(2*alpha_coefficient,1), round(3*alpha_coefficient, 1), round(extra_mapped_penalty, 1)
beta0, beta1, beta2, beta3, beta4 = 0, round(0.1*beta_coefficient,2), round(0.2*beta_coefficient,2), round(0.3*beta_coefficient,2), round(0.5*beta_coefficient, 2)
suspicious_penalty_ratio = 0.2
mapped_suspicious_penality = 5 

### A rough classification of availability based on road type
class0 = ["cycleway"]
class1 = ["residential", "tertiary" ]
class2 = ["service", "path", "living_street"]
class3 = ["secondary","footway",  "unclassified", "services", "pedestrian", "steps", "bridleway", "busway", "platform", "construction"]
class4 = ['primary', 'primary_link', 'secondary_link', 'motorway', 'motorway_link']

### Bikeable and unbikeable road types based on disrepancy between OSM and real-world observation
unconfirmed_roads_type = ["secondary", "tertiary"]
confirmed_car_roads_type=["primary", "primary_link", "secondary_link", "motorway", "motorway_link"]
bikable_type = ["cycleway", "footway", "residential"] 
unbikeable_type = ["primary", "primary_link","tertiary", "secondary", "secondary_link", "motorway", "motorway_link"]
max_km_coefficient = 2 # searching distance expanding threshold for each road type
max_depth = 5 # searching depth threshold

### Rules
case1_theta = 1.2 # detour ratio threshold in case 1
case2_theta = 2 # detour ratio threshold in case 2
case3_delta = 0.02 # detour distance threshold in case 2, in km
frontier_exception_nodes = [19466] #Nodes that are in the frontier of network and do not contain alternative bikeable roads nearby

### Hyperparameters for the special case(a)
ver_invser_km_dict = {i:0.3 for i in unbikeable_type}
ver_invser_km_dict["tertiary"]=0.2

### Hyperparameters for the special case(b)
network_interrupt_threshold = 3