import geopandas as gpd
import pandas as pd
import numpy as np
from utils import *
from shapely.geometry import LineString

try:
    from config_mapmatch import *
except ModuleNotFoundError:
    raise Warning('config file for map matchingnot found')
 

def transform_osm():
    '''
    Input: OpenStreetMap
    Output: Each segment of the road is a separate edge with source and target node, with its corresponding features
    The source and target nodes and their corresponding coordinates are stored in a point_save_path
    '''
    osm_df = gpd.read_file(raw_osm_file)
    puntid_dict, idpunt_dict, _, _ = get_map_dict(osm_df, point_save_path)
    columns_ = [i for i in osm_df.columns if i!="geometry"]
    data = {i:[] for i in columns_+["source", "target", "km"]}

    geometry = []
    for index, row in osm_df.iterrows():
        points = list(row['geometry'].coords)
        for i in columns_:
            data[i] += [row[i] for j in range(len(points)-1)]
        for j in range(len(points)-1):
            data["source"].append(puntid_dict[str(points[j])])
            data["target"].append(puntid_dict[str(points[j+1])])
            data["km"].append(geo_dist_single(points[j], points[j+1])/1000)
            geometry.append(LineString([points[j], points[j+1]]))
    data["geometry"] = geometry
    df = gpd.GeoDataFrame(data, geometry='geometry')
    df.crs = 'EPSG:4326'

    df["osm_id"] = range(len(df))
    df.rename(columns={"maxspeed":"kmh", "osm_id":"edge"}, inplace=True)
    df.drop(columns=["old_osm_id", "name", "ref"], inplace=True)
    df = df.set_index('edge')
    return df

def adjacent_similar_edges(raw_df):
    '''
    Input: formulated OpenStreetMap dataframe
    Output: dictionary of adjacent and similar edges d[start_edge]=next_edge
    '''
    ### Get (start point -> edge) and (end point -> edge) dictionary
    edge_pts_dict={i:[j,k] for i,j,k in zip(raw_df.index, raw_df.source, raw_df.target)}
    all_pts = np.unique(raw_df[['source', 'target']].values)
    start_pts_edge, end_pts_edge, connection_dict={i:[] for i in all_pts}, {i:[] for i in all_pts}, {i:[] for i in raw_df.index}
    for edge in edge_pts_dict.keys():
        start, end=edge_pts_dict[edge]
        start_pts_edge[start].append(edge)
        end_pts_edge[end].append(edge)

    ### We only consider two edges are possible to be merged 
    ### Satify the following conditions: (1) they are not connected by intersection point (2) same road type and same direction information
    one_way, street_type = raw_df.oneway.to_numpy(), raw_df['type'].to_numpy()
    for edge in raw_df.index:
        end_point=edge_pts_dict[edge][-1]
        if len(start_pts_edge[end_point])==1 and len(end_pts_edge[end_point])==1:
            if one_way[edge]==one_way[start_pts_edge[end_point][0]] and street_type[edge]==street_type[start_pts_edge[end_point][0]]:
                connection_dict[edge].append(start_pts_edge[end_point][0])
    return connection_dict

def merge_edge_features(merged_edges, raw_df):
    '''
    Objective: derive the features for the simplified graph
    '''
    coarse2full_edge = {i:j for i, j in enumerate(merged_edges)}
    full2coarse_edge = {}
    for key in coarse2full_edge.keys():
        for item in coarse2full_edge[key]:
            full2coarse_edge[item]=key
    full2coarse_edge, coarse2full_edge = full2coarse_edge, coarse2full_edge

    # Get the accumulated distance for each edge, the accumulated distance is the distance for all neighbor edges before
    accumulated_distance, accumulated_index ={i:0 for i in raw_df.index}, {i:0 for i in raw_df.index}
    kms = raw_df['km'].to_numpy()
    for edges in merged_edges:
        for idx in range(1,len(edges)):
            accumulated_index[edges[idx]]=idx
            accumulated_distance[edges[idx]]=kms[edges[idx-1]]+accumulated_distance[edges[idx-1]]
    raw_df['accu_dist']=raw_df.index.map(accumulated_distance)
    raw_df['accu_idx']=raw_df.index.map(accumulated_index)
    raw_df['c_edge']=raw_df.index.map(full2coarse_edge)
    raw_df["edge"]=raw_df.index
    raw_df.to_file(complete_graph_path, index=False)
    raw_df = raw_df.sort_index()
    raw_df.set_index("edge", inplace=True)

    # Build the simplified graph
    one_way, street_type = raw_df.oneway.to_numpy(), raw_df['type'].to_numpy()
    target, source = raw_df['target'].to_numpy(), raw_df['source'].to_numpy()
    simplified_df=pd.DataFrame({'edge':coarse2full_edge.keys()})
    simplified_df['source']=simplified_df['edge'].map({i:source[coarse2full_edge[i][0]] for i in coarse2full_edge})
    simplified_df['target']=simplified_df['edge'].map({i:target[coarse2full_edge[i][-1]] for i in coarse2full_edge})
    simplified_df['km']=simplified_df['edge'].map({i:accumulated_distance[coarse2full_edge[i][-1]]+kms[coarse2full_edge[i][-1]] for i in coarse2full_edge})
    simplified_df['oneway']=simplified_df['edge'].map({i:one_way[coarse2full_edge[i][0]] for i in coarse2full_edge})
    simplified_df['type']=simplified_df['edge'].map({i:street_type[coarse2full_edge[i][0]] for i in coarse2full_edge})
    return raw_df, simplified_df

def define_cost(simplified_df):
    # Define the travel cost as the distance in meters
    simplified_df["cost"]=simplified_df["km"]*1000
    # Define the cost based on rough classification of availability
    beta_list = [beta0, beta1, beta2, beta3, beta4]
    class_list = [class0, class1, class2, class3, class4]
    for class_, beta_ in zip(class_list, beta_list):
        simplified_df.loc[(simplified_df["type"].isin(class_)), "cost"] = simplified_df.loc[(simplified_df["type"].isin(class_)), "cost"]*(1+beta_)
    simplified_df["reverse_cost"]=simplified_df["cost"]*(1+revser_coeffi*simplified_df["oneway"])
    simplified_df = simplified_df.sort_values('edge')
    return simplified_df

def simplify_graph():
    def recursive_programming(parent, edge, d):
        '''
        Objective: merge all the adjacent edges with same characteristics into one edge
        '''
        if d[edge]==-1 or len(d[edge])==0:
            return parent, d
        next_edge=d[edge][0]
        parent=parent+[edge]
        if next_edge in parent: # get rid of loop
            return parent, d
        parent, d=recursive_programming(parent, next_edge, d)
        if d[next_edge]!=-1:
            d[edge]+=d[next_edge]
        d[next_edge]=-1
        return parent, d
    
    raw_df = transform_osm()
    d = adjacent_similar_edges(raw_df)
    for edge in raw_df.index:
        _, d=recursive_programming([], edge, d)
    merged_edges = [[key]+d[key] for key in d.keys() if d[key]!=-1]
    raw_df, simplified_df = merge_edge_features(merged_edges, raw_df)
    simplified_df = define_cost(simplified_df)
    simplified_df.to_csv(simplified_graph_path, index=False)
    simplified_df.set_index("edge", inplace=True)
    return raw_df, simplified_df

    # Separate a linestring into segments
    
    

if __name__ == "__main__":
    '''
    This file aims to build a simplified graph from the raw OpenStreetMap data
    Adjacent road segments with similar characteristics are merged into one edge
    Adjacent road segments means their connecting nodes only have one incoming and one outgoing road segment
    '''
    simplify_graph()
    print("Graph Simplification Completed, Saved at", simplified_graph_path)
