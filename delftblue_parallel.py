import os
import time
import glob
import json
import math
import sys
import datetime
import networkx as nx
import geopandas as gpd
import pandas as pd
import numpy as np
import scipy.sparse as sp
from mpi4py import MPI
from scipy.sparse import csr_matrix
from utils import *
from scipy import stats, sparse
from geopy.distance import geodesic
from shapely.geometry import LineString


try:
    from config_mapmatch import *
except ModuleNotFoundError:
    raise Warning('config file for map matchingnot found')

def tracetable(inputfile):
    df = read_h5(inputfile)
    df = df.sort_values(by=["tripID", "timestamp"])
    df["timestamp"] = (df.timestamp-datetime.datetime(2020, 10, 1)).dt.total_seconds()
    df = df.reset_index(drop=True)
    return df

def DelftBlue_getsPts(tripIDList, all_data, start, end, full_edgesDf, edgesDf, myrank):
    """ Get all points on GPS traces, and edges within the search radius of each"""
    final_results = []
    coarse_edge_num, full_edge_num = len(edgesDf), len(full_edgesDf)
    coarse_edge_idx = full_edgesDf.c_edge.to_numpy()
    accumulated_dist = full_edgesDf.accu_dist.to_numpy()
    all_dist = edgesDf.km.to_numpy()
    write_log("** PtsDf Process "+str(myrank)+".\n", mode="a")
    for tripID in tqdm(tripIDList, desc="GetPtsDf Process "+str(myrank), unit="item", mininterval=100):
        traceSegment = all_data[all_data["tripID"]==tripID].copy()
        points = np.concatenate([np.expand_dims(traceSegment.lon, axis=-1), np.expand_dims(traceSegment.lat, axis=-1)], axis=-1)
        traceSegment["seglength"] = np.append(0, geo_dist_arr(points[:-1, :], points[1:, :]))/1000

        dup_start, dup_end = np.tile(start, [len(points), 1, 1]), np.tile(end, [len(points), 1, 1]) #(nb_points, nb_roads, 2)
        dup_points = np.repeat(np.expand_dims(points, axis=1), repeats=dup_start.shape[1], axis=1) #(nb_points, nb_roads, 2)
        
        a = geo_dist(dup_points, dup_start)
        c = geo_dist(dup_points, dup_end)
        b = geo_dist(dup_start, dup_end)
        theta = np.arccos(np.clip((a**2+b**2-c**2)/(2*a*b), -1, 1))
        dist = a*np.sin(theta)
        t = a*np.cos(theta) #the projection
        dist = np.where(theta>np.pi/2, a, dist)
        dist = np.where(t>b, c, dist)
        t[theta>np.pi/2]=0
        t = np.where(t>b, b, t)

        n_id = len(dist)
        accumulated_d = np.repeat(np.expand_dims(accumulated_dist, axis=0), repeats=n_id, axis=0) #km
        accumu_t = t/1000+accumulated_d #km

        all_index = np.repeat(np.arange(full_edge_num)[np.newaxis, :], n_id, axis=0).astype(int)
        new_dist = np.zeros((len(dist), coarse_edge_num))
        frac = np.zeros((len(dist), coarse_edge_num))
        new_dist_arg = np.zeros((len(dist), coarse_edge_num)).astype(int)
        for coarse_edge in range(coarse_edge_num):
            sub_arr = dist[:, coarse_edge_idx==coarse_edge]
            new_dist[:, coarse_edge] = np.min(sub_arr, axis=1)
            arg_index = np.argmin(sub_arr, axis=1)
            new_dist_arg[:, coarse_edge] = all_index[:, coarse_edge_idx==coarse_edge][range(n_id), arg_index]
            frac[:, coarse_edge] = accumu_t[:, coarse_edge_idx==coarse_edge][range(n_id), arg_index]/all_dist[coarse_edge]
        
        frac[frac>1]=1
        selected_points, selected_coarse_edges = np.where(new_dist<=50)
        selected_traceSegment = traceSegment.iloc[selected_points]
        if len(selected_traceSegment.index.unique())<=2:
            continue
        
        ptsDf = pd.DataFrame({'nid': selected_traceSegment.index, 'tripID': selected_traceSegment.tripID, 'edge': selected_coarse_edges, 'full_edge':new_dist_arg[(selected_points, selected_coarse_edges)], 'dist': new_dist[(selected_points, selected_coarse_edges)],
                        'frcalong': frac[(selected_points, selected_coarse_edges)], 'secs': selected_traceSegment.timestamp, 'seglength':selected_traceSegment.seglength})

        ptsDf = ptsDf.set_index("nid")

        if not len(np.unique(ptsDf.index)) == ptsDf.index.max()+1:  # renumber nids to consecutive range
            lookup = dict(zip(np.unique(ptsDf.index), np.arange(ptsDf.index.max()+1)))
            ptsDf.reset_index(inplace=True)
            ptsDf['nid'] = ptsDf.nid.map(lookup)
            ptsDf.set_index('nid', inplace=True)

        ptsDf['distprob'] = ptsDf.dist.apply(lambda x: distanceLL(x))
        nids = ptsDf.index.unique()
        ptsDf.loc[nids.max(), 'distprob'] *= weight_1stlast  # first point is dealt with in viterbi
        ptsDf['rownum'] = list(range(len(ptsDf)))
        final_results.append(ptsDf)

    store_h5(outputdir+str(myrank)+".h5", pd.concat(final_results))
    return 


def DelftBlue_matchTrace(map_graph, edgesDf, h5_folder,uturncost, myrank):
    maxN = max(edgesDf.source.max(), edgesDf.target.max()) + 1
    costMatrix = sparse.dok_matrix((maxN, maxN))
    distMatrix = sparse.dok_matrix((maxN, maxN))
    # penaltyMatrix = sparse.dok_matrix((maxN, maxN))

    def shortest_path(node_src, node_tgt):
        n = costMatrix.get((node_src, node_tgt), -1)
        if n!=-1:
            return n, distMatrix.get((node_src, node_tgt), -1)#, penaltyMatrix.get((node_src, node_tgt), -1)
        try:
            shortest_path_list=nx.shortest_path(map_graph, source=node_src, target=node_tgt, weight='cost', method='dijkstra')
            shortest_distance, shortest_cost, penality=0, 0, 0
            for i in range(len(shortest_path_list) - 1):
                km_ = map_graph[shortest_path_list[i]][shortest_path_list[i + 1]]['distance']
                shortest_distance+=km_
                shortest_cost+=map_graph[shortest_path_list[i]][shortest_path_list[i + 1]]['cost']
                # penality+=max(km_*10, 1)*penality_dict[map_graph[shortest_path_list[i]][shortest_path_list[i + 1]]['roadtype']]
        except:   
            shortest_distance=10000000
            shortest_cost=10000000
            # penality=10000000
        costMatrix[node_src, node_tgt]=shortest_cost
        distMatrix[node_src, node_tgt]=shortest_distance
        # penaltyMatrix[node_src, node_tgt]=penality

        return shortest_cost, shortest_distance#, penality

    def transProb(rr1, rr2, dir1, dir2, sl, uturncost, default_value=-1):
        """Returns input to likelihood functions for transition between edges
            (rr1.edge!=rr1.edge)"""
        if dir1 == 0:
            frc1 = 1-rr1['frcalong']   # frc of edge remaining
            n0 = rr1['source']
            n1 = rr1['target']
            e1cost = rr1['cost']
        else:
            frc1 = rr1['frcalong']
            n0 = rr1['target']
            n1 = rr1['source']
            e1cost = rr1['reverse_cost']

        if dir2 == 0:
            frc2 = rr2['frcalong']  # frc of edge that will be traveled
            n2 = rr2['source']
            n3 = rr2['target']
            e2cost = rr2['cost']
        else:
            frc2 = 1-rr2['frcalong']
            n2 = rr2['target']
            n3 = rr2['source']
            e2cost = rr2['reverse_cost']

        # routing cost is cost of 1st edge + routing cost + uturn cost + cost of last edge. e1!=e2 needed in case there are self-loops
        
        n0, n1, n2, n3 = int(n0), int(n1), int(n2), int(n3)
        rc, _ = shortest_path(n1, n2)#penality_rc  # routing cost
        
        n1_n0, n0_n2, n1_n3, n1_n2, n0_n3 = shortest_path(n1, n0), shortest_path(n0, n2), shortest_path(n1, n3), shortest_path(n1, n2), shortest_path(n0, n3)
        uturn = -1
        if rr1['edge'] == rr2['edge'] and dir1 != dir2:
            uturn = 1
            if frc1 > 0.95 and frc2 > 0.95:
                frc1, frc2 = 50, 50  # haven't started the edge and already want to do a U-turn? Disallow
            elif frc1 > frc2:  # continuing along original path, then U turn
                frc1, frc2 = frc1-frc2, 0
            else:  # U turn, then backtrack
                frc1, frc2 = 0, frc2-frc1
            travelled_dist = rr1['km']*frc1+rr2['km']*frc2
            
        elif rr1['edge'] != rr2['edge']:
            e1Uturn = np.round(rc, 8) == np.round(n1_n0[0]+n0_n2[0], 8)
            e2Uturn = np.round(rc, 8) == np.round(n1_n3[0]+rr2[['reverse_cost', 'cost'][dir2]], 8)

            if e2Uturn:  # U-turn on e2, disallow Uturns on both (causes problems in a grid setting)
                uturn = 2
                e2cost = rr2[['reverse_cost', 'cost'][dir2]]
                frc2 = 1-frc2
                frc2_d = frc2
                if frc2 < 0.05: 
                    frc2 = 1000  # haven't started the edge and already want to do a U-turn? Disallow
            if e1Uturn:  # u-turn on e1
                uturn = 3
                e1cost = rr1[['reverse_cost', 'cost'][dir1]]
                frc1 = 1-frc1
                frc1_d = frc1
                if frc1 < 0.05: 
                    frc1 = 1000  # haven't started the edge and already want to do a U-turn? Disallow
            if e1Uturn and e2Uturn:
                uturn = 4  # note this attracts a greater uturncost
        else:
            somemistake

        if sl == 0 or uturn == 1:  # Uturn on e1, but same edge as e2
            dratio = 1
        else:  # note that frc1 and frc2 have already been changed in the event of a Uturn
            if uturn == -1:  # no Uturn
                networkDist = n1_n2[1]+rr1['km']*frc1+rr2['km']*frc2
            elif uturn == 2:  # Uturn on e2
                networkDist = n1_n3[1]+rr1['km']*frc1+rr2['km']*frc2
            elif uturn == 3:   # Uturn on e1
                networkDist = n0_n2[1]+rr1['km']*frc1+rr2['km']*frc2
            elif uturn == 4:   # Uturn on both
                networkDist = n0_n3[1]+rr1['km']*frc1+rr2['km']*frc2
            else:
                somemistakehere
            dratio = networkDist*1./sl
        
        if uturn == -1:
            travelled_dist = n1_n2[1]+rr1['km']*frc1+rr2['km']*frc2
        if uturn == 2:
            travelled_dist = n1_n3[1]+rr1['km']*frc1+rr2['km']*(2-frc2_d)
        if uturn == 3:
            travelled_dist = n0_n2[1]+rr1['km']*(2-frc1_d)+rr2['km']*frc2
        if uturn == 4:
            travelled_dist = n0_n2[1]+rr1['km']*(2-frc1_d)+rr2['km']*(2-frc2_d)

        # cratio = penality_rc+ max(rr1['km']*10*frc1, 1)*penality_dict[rr1['type']]+ max(rr2['km']*10*frc2, 1)*penality_dict[rr2['type']]+
        cratio = penality_dict[rr1['type']]+penality_dict[rr2['type']]+rr1["penality"]*extra_mapped_penalty+rr2["penality"]*extra_mapped_penalty+(e1cost*frc1+rc+uturncost*(uturn > 0)+uturncost*2*(uturn == 4)+e2cost*frc2)/max(1, rr2['secs']-rr1['secs'])
        # if sl==0 and uturn!=1:
        #     travelled_dist=0
        speed = travelled_dist*1000./(rr2['secs']-rr1['secs'])

        # The values will be in a sparse matrix, so zeros aren't distinguishable from empty arrays
        return max(cratio, 1e-10), max(dratio, 1), uturn, max(speed, 1e-10)
    

    def transProbSameEdge(rr1, rr2, dir, sl):
        """Returns input to likelihood functions for movement along an edge
        (rr1.edge==rr1.edge)"""
        frc = abs(rr2['frcalong']-rr1['frcalong']) if dir == 0 else abs(rr1['frcalong']-rr2['frcalong'])
        distratio = 1 if sl == 0 else rr1['km']*frc*1./sl
        speed = rr1['km']*frc*1000./(rr2['secs']-rr1['secs'])
        # cratio = max(rr1['km']*10*frc, 1)*penality_dict[rr1['type']]+rr1[['cost', 'reverse_cost'][dir]]*frc/max(1, rr2['secs']-rr1['secs'])
        cratio = penality_dict[rr1['type']]+penality_dict[rr2['type']]+rr1["penality"]*extra_mapped_penalty+rr2["penality"]*extra_mapped_penalty+rr1[['cost', 'reverse_cost'][dir]]*frc/max(1, rr2['secs']-rr1['secs'])
        return max(cratio, 1e-10), max(distratio, 1), -1, max(speed, 1e-10)

    def viterbi(N, nids, ptsDf, temporalScores, topologicalScores):
        """Derived from https://phvu.net/2013/12/06/sweet-implementation-of-viterbi-in-python/
        Key changes are to work in log space, make use of self.matrices, and use sparse representations to save memory"""
        # obs=None simply steps through the K state probabilities in order, i.e. K = self.obsProb.shape[1]
        # if obs is None: obs=range(self.obsProb.shape[1])

        # to distinguish true zeros from empty cells, we will add 1 to all values here
        backpt = sparse.lil_matrix((N, nids.max()+1), dtype=np.int32)

        # initialization
        trellis_0 = (np.repeat(ptsDf.loc[[0], 'distprob'], 2) * weight_1stlast).values
        lastidx1, lastidx2 = 0, trellis_0.shape[0]
        for nid in nids[1:]:

            nToSkip = 0 if nid == len(nids)-1 else max_skip  # Don't allow last point to be dropped, so set other probabilities very small

            idx1 = int(ptsDf.loc[max(0, nid-nToSkip), 'rownum'].min()*2)
            idx2 = int(ptsDf.loc[nid, 'rownum'].max()*2+2)
            iterN = idx2-idx1  # npoints on this iteration

            # calculate the probabilities from the scores matrices
            transScoreArr = temporalScores[lastidx1:lastidx2, idx1:idx2].toarray()
            transScoreArr[transScoreArr == 0] = 1e10  # these are not true zeros, but empty cells
            distratioArr = topologicalScores[lastidx1:lastidx2, idx1:idx2].toarray()
            distratioArr[distratioArr == 0] = 1e10  # these are not true zeros, but empty cells
            speedArr = speedScores[lastidx1:lastidx2, idx1:idx2].toarray()
            speedArr[speedArr == 0] = 1e10  # these are not true zeros, but empty cells

            # print(np.round(transScoreArr,  3), np.round(temporalLL(transScoreArr), 3))
            # print("--"*12)
            LL = temporalLL(transScoreArr) + topologicalLL(distratioArr) + speedLL(speedArr)

            # 1st term is the probability from the previous iteration. 2nd term is observation probability. 3rd term is the transition probability
            trellis_1 = np.nanmax(np.broadcast_to(trellis_0.reshape(-1, 1), (trellis_0.shape[0], iterN)) +
                                (np.repeat(ptsDf.loc[nid-nToSkip:nid, 'distprob'], 2)-skip_penalty*np.repeat(np.array(nid-ptsDf.loc[nid-nToSkip:nid].index)**2, 2)).values.reshape(-1, 1).T +
                                LL, 0)
            # print(nid, LL.shape, trellis_1.shape)
            backpt[idx1:idx2, nid] = np.nanargmax(np.broadcast_to(trellis_0.reshape(-1, 1), (trellis_0.shape[0], iterN)) + LL, 0).reshape(-1, 1) + lastidx1 + 1

            trellis_0 = trellis_1
            lastidx1, lastidx2 = idx1, idx2

        # termination
        tokens = [np.nanargmax(trellis_1)+idx1]
        for i in reversed(nids[1:]):
            tokens.append(backpt[tokens[-1], i]-1)
        return tokens[::-1]

    def fillRouteGaps(route, uturns, nids, ptsDf):
        """Takes the top route in the list (must be sorted!) and fills in gaps"""
        # remove duplicates
        keepPts = [0]+[ii+1 for ii, rr in enumerate(route[1:]) if route[ii] != rr]
        edgeList = [rr[0] for ii, rr in enumerate(route) if ii in keepPts]
        nodeList = [int(edgesDf.loc[rr[0], 'target']) if rr[1] == 0 else
                    int(edgesDf.loc[rr[0], 'source'])
                    for ii, rr in enumerate(route) if ii in keepPts]
        uturnList = [rr for ii, rr in enumerate(uturns) if ii+1 in keepPts]

        if len(edgeList) == 1:
            bestRoute = edgeList
            uturnFrcs = []
            return bestRoute, uturnFrcs, keepPts

        starttime = time.time()

        firstEdge = True
        fullroute = [edgeList[0]]  # first edge
        uturnFrcs = [-1]
        keepPts.append(len(nids))  # this is for indexing (below)
        oNode = nodeList[0]  # origin for next route
        for ii, (edge, node, uturn, ptId) in enumerate(zip(edgeList[1:], nodeList[1:], uturnList, keepPts[1:-1])):
            # node is far end of the edge we are going to. We want the other end, called dNode
            if uturn in [3, 4]:  # uturn on first edge
                fullroute.append(fullroute[-1])
                frcs = ptsDf.loc[keepPts[ii]:keepPts[ii+1]-1].loc[ptsDf.loc[keepPts[ii]:keepPts[ii+1]-1].edge == route[ptId-1][0], 'frcalong']
                uturnFrcs = uturnFrcs[:-1]+[(frcs.min(), frcs.max()), -1]
                oNode = edgesDf.target[fullroute[-1]] if edgesDf.source[fullroute[-1]] == oNode else edgesDf.source[fullroute[-1]]
            if uturn in [2, 4]:  # uturn on last edge
                dNode = node
            else:
                dNode = edgesDf.target[edge] if edgesDf.source[edge] == node else edgesDf.source[edge]

            if oNode != dNode:  # missing edges
                try:
                    # print("REWRITE fillROUTEGAPS")
                    shortest_path = nx.shortest_path(map_graph, source=oNode, target=dNode, weight='cost')
                    result=[]
                    for i in range(len(shortest_path)-1):
                        result.append(map_graph[shortest_path[i]][shortest_path[i+1]]["index"])
                        # try: 
                        #     result.append(edge_dict[str((shortest_path[i],shortest_path[i+1]))])
                        # except:
                        #     result.append(edge_dict[str((shortest_path[i+1],shortest_path[i]))])
                    fullroute += result
                    uturnFrcs += [-1]*(len(result))
                # add route where pgr_dijkstra does not return a result, usually because of islands
                except nx.NetworkXNoPath:
                    print('Could not match trace. Network may be disconnected or have islands.')
                    bestRoute = None
                    uturnFrcs = None
                    return None, None, None
            if uturn in [2, 4]:
                fullroute.append(edge)
                frcs = ptsDf.loc[keepPts[ii+1]:keepPts[ii+2]].loc[ptsDf.loc[keepPts[ii+1]:keepPts[ii+2]].edge == route[ptId][0], 'frcalong']
                uturnFrcs.append((frcs.min(), frcs.max()))
            fullroute.append(edge)
            if uturn == 1:
                frcs = ptsDf.loc[keepPts[ii]:keepPts[ii+2]-1].loc[ptsDf.loc[keepPts[ii]:keepPts[ii+2]-1].edge == route[ptId][0], 'frcalong']
                if fullroute[-1] == fullroute[-2] and uturnFrcs[-1] == -1:  # uturn already made on previous edge, but not counted
                    uturnFrcs = uturnFrcs[:-1]+[(frcs.min(), frcs.max()), -1]
                else:
                    uturnFrcs.append((frcs.min(), frcs.max()))
            else:
                uturnFrcs.append(-1)

            oNode = node   # starting point for next edge

        # Check whether there is a U-turn on the final edge -  issue #12
        if allowFinalUturn:
            for nid in reversed(nids[1:]):  # find out where the final edge starts
                if route[nid]!=route[nid-1]:
                    break
            frcsAlong = ptsDf[ptsDf.edge==route[-1][0]].loc[nid:,'frcalong']
            threshold = 0.1 # how many km the furthest GPS ping has to be along, in order to add a uturn
            if ((route[-1][1] == 0 and frcsAlong.max()*edgesDf.loc[route[-1][0],'km'] > threshold) or
                (route[-1][1] == 1 and (1-frcsAlong.min())*edgesDf.loc[route[-1][0],'km'] > threshold)):  
                fullroute.append(fullroute[-1])
                uturnFrcs[-1] = (frcsAlong.min(), frcsAlong.max())
                uturnFrcs.append(-1)

        assert len(uturnFrcs) == len(fullroute)
        bestRoute = fullroute
        uturnFrcs = uturnFrcs
        return bestRoute, uturnFrcs, keepPts[:-1]

    
    all_dfs, all_str=[], []
    data = read_h5(h5_folder+str(myrank)+".h5", time_converse=False)
    data = pd.merge(data, edgesDf, how='left', left_on='edge', right_index=True)
    tripIDList = data.tripID.unique()

    with open(outputdir+str(myrank)+".txt", 'w') as file:
        file.write('tripID, edge, dist_mean, dist_min, dist_max, speed_mean, speed_min, speed_max, topologicalScore_mean, topologicalScore_min, topologicalScore_max, dist_proba_mean, dist_proba_min, dist_proba_max, speed_proba_mean, speed_proba_min, speed_proba_max, topologicalScore_proba_mean, topologicalScore_proba_min, topologicalScore_proba_max\n') 
    write_log("** MatchTrace Process "+str(myrank)+".\n", mode="a")

    for tripID in tqdm(tripIDList, desc="Match Trace Process "+str(myrank), unit="item", mininterval=60*10):
        starttime = time.time()
        
        ptsDf = data[data["tripID"]==tripID].copy()

        # ptsDf["distprob"]=ptsDf.dist.apply(distanceLL)
        nids = ptsDf.index.unique()
        if len(nids) < 3:
            print('Skipping match. Need at least 3 points in trace')
            continue

        N = len(ptsDf)*2
        t = 0

        rowKeys, colKeys, scores = [], [], []   # scores are held here, before conversion to a sparse matrix
        for nid1, rr1 in ptsDf.loc[:nids[-2]].iterrows():
            rr1 = rr1.to_dict()
            # iterrows loses the dtype of the ints (converts them to floats), and only ints can index a sparse matrix
            for dir1 in [0, -1]:
                idx1 = rr1['rownum']*2-dir1
                # fill diagonal
                rowKeys.append(idx1)
                colKeys.append(idx1)
                scores.append((1e-10, 1, -1, -1))
                seglength, lastnid = 0., -1
                for nid2, rr2 in ptsDf.loc[nid1+1:nid1+1+max_skip].drop_duplicates('edge').iterrows():  # max_skip is the maximum number of rows to skip. We drop duplicates because if this edge was done at a previous nid, we can skip
                    rr2 = rr2.to_dict()
                    if nid2 != lastnid:  # update seglength if a new nid is being entered, and pass it to the scores functions
                        seglength += rr2['seglength']
                        lastnid = nid2
                    for dir2 in [0, -1]:
                        rowKeys.append(idx1)
                        colKeys.append(int(rr2['rownum']*2-dir2))
                        if rr1['edge'] == rr2['edge'] and dir1 == dir2:
                            scores.append(transProbSameEdge(rr1, rr2, dir1, seglength))
                        elif rr1['edge'] == rr2['edge'] or rr1[['target', 'source'][dir1]] != rr2[['target', 'source'][dir2]]:
                            scores.append(transProb(rr1, rr2, dir1, dir2, seglength, uturncost))
                        else:
                            scores.append((1e10, 1e10, -1, -1))

        # coo matrix from lists is fastest way to build a sparse matrix,
        # rather than assigning to lil or dok matrix directly
        # then convert to csr, which is more efficient for matrix algebra
        temporalScores = sparse.coo_matrix(([ii[0] for ii in scores], (rowKeys, colKeys)), shape=(N, N), dtype=np.float32).tocsr()
        topologicalScores = sparse.coo_matrix(([ii[1] for ii in scores], (rowKeys, colKeys)), shape=(N, N), dtype=np.float32).tocsr()
        uturns = sparse.coo_matrix(([ii[2] for ii in scores], (rowKeys, colKeys)), shape=(N, N), dtype=np.int8).tocsr()
        speedScores= sparse.coo_matrix(([ii[3] for ii in scores], (rowKeys, colKeys)), shape=(N, N), dtype=np.float32).tocsr()
        

        # if d has duplicate value, then some nids are skipped, so len(d)=len(nids)
        d = viterbi(N=N, nids=nids, ptsDf=ptsDf, temporalScores=temporalScores, topologicalScores=topologicalScores)
    
        route = [(ptsDf.loc[ptsDf.rownum == int(dd/2), 'edge'].values[0], -1*dd % 2) for dd in d]  # tuple of (edge, direction)
        uturns = [uturns[n1, n2] for n1, n2 in zip(d[:-1], d[1:])]
        speeds = [speedScores[n1, n2] for n1, n2 in zip(d[:-1], d[1:])]

        bestRoute, uturnFrcs, keepPts_ = fillRouteGaps(route, uturns, nids, ptsDf)
        if bestRoute is None or -1 in bestRoute:
            continue
        keepPts = [0]+[ii+1 for ii, rr in enumerate(d[1:]) if d[ii] != rr]

        selected_row = np.asarray([int(dd/2) for dd in d])[keepPts]
        selected_ptsDf = ptsDf.loc[ptsDf.rownum.isin(selected_row)].copy()

        selected_d = np.asarray(d)[keepPts]
        
        temporalScore_lst = [temporalScores[n1, n2]for n1, n2 in zip(selected_d[:-1], selected_d[1:])]
        topologicalScore_lst = [topologicalScores[n1, n2] for n1, n2 in zip(selected_d[:-1], selected_d[1:])]

        selected_ptsDf['uturn']=[rr for ii, rr in enumerate(uturns) if ii+1 in keepPts]+[-1]
        selected_ptsDf['speed_'] = [rr for ii, rr in enumerate(speeds) if ii+1 in keepPts]+[-1]
        selected_ptsDf['dir'] = [rr[1] for ii, rr in enumerate(route) if ii in keepPts]
        selected_ptsDf["tmpScore"]=temporalScore_lst+[-1]
        selected_ptsDf["topoScore"]=topologicalScore_lst+[-1]

        if selected_ptsDf["speed_"].iloc[0]>20:
            selected_ptsDf = selected_ptsDf.iloc[1:]
            for i in range(len(bestRoute)):
                if bestRoute[i]==selected_ptsDf.edge.iloc[0]:
                    bestRoute = bestRoute[i:]
                    break

        if len(selected_ptsDf)<2:
            print('Abandoned match. Need at least 2 points within one trace')
            continue

        dist_lst = selected_ptsDf.dist.tolist()
        dist_ = [np.mean(dist_lst), np.min(dist_lst), np.max(dist_lst)]
        distLLs = selected_ptsDf.distprob.describe()[['mean', 'min', 'max']].tolist()

        temporalScore_lst = selected_ptsDf.tmpScore.tolist()[:-1]
        topologicalScore_lst = selected_ptsDf.topoScore.tolist()[:-1]
        temporalLLarray = temporalLL(temporalScore_lst)
        topologicalLLarray = topologicalLL(topologicalScore_lst)

        t_ = [np.mean(temporalScore_lst), np.min(temporalScore_lst), np.max(temporalScore_lst)]
        topo_ = [np.mean(topologicalScore_lst), np.min(topologicalScore_lst), np.max(topologicalScore_lst)]
        t_prob = [np.mean(temporalLLarray), np.min(temporalLLarray), np.max(temporalLLarray)]
        topo_prob = [np.mean(topologicalLLarray), np.min(topologicalLLarray), np.max(topologicalLLarray)]

        LL = dist_+t_+topo_+[i for i in distLLs]+t_prob+topo_prob
        rlt = ",".join([str(tripID)]+[" ".join([str(x) for x in bestRoute])]+[str(round(x, 3)) for x in LL])
        all_str.append(rlt)
        all_dfs.append(selected_ptsDf)
    
    store_h5(outputdir+'viterbi/'+str(myrank)+".h5", pd.concat(all_dfs))
    with open(outputdir+str(myrank)+".txt", 'a') as file:
        file.write("\n".join(all_str))
    return

def graph_construct(df):
    oneway = df.drop(columns=["reverse_cost"]).copy()
    reverse = df.drop(columns=["cost"]).copy().rename(columns={"reverse_cost":"cost", "target":"source", "source":"target"})
    twoway = pd.concat([oneway, reverse]).sort_values(by="cost")
    twoway = twoway.drop_duplicates(subset=["source", "target"], keep="first")
    twoway = twoway.sort_index()
    df = twoway.copy()

    G=nx.DiGraph()
    for idx, row in df.iterrows():
        source = row['source']
        target = row['target']
        cost = row['cost']
        km = row["km"]
        type_ = row["type"]
        G.add_edge(source, target, cost=cost, distance=km,  roadtype=type_, index=idx)
    return G

if __name__ == "__main__":
    function_name = sys.argv[1]
    

    if function_name == "DelftBlue_getsPts":
        input_file = sys.argv[2] 
        outputdir = sys.argv[3]  
        street_map = "mapMatch_result/full_roads.shp"
        coarse_street_map = "mapMatch_result/penalized_coarse_roads.csv"

        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        outputdir = outputdir

        full_edgesDf = gpd.read_file(street_map)
        full_edgesDf.set_index("edge", inplace=True)
        edgesDf = pd.read_csv(coarse_street_map)
        edgesDf.set_index("edge", inplace=True)

        # data = tracetable(input_file)
        data = pd.read_csv(input_file)
        all_trips = data.tripID.unique()
        all_trips.sort()
        all_coords = np.asarray([list(point) for linestring in full_edgesDf.geometry for point in linestring.coords]).flatten()
        all_coords = all_coords.reshape((-1, 2, 2))
        start,end = all_coords[:,0,:], all_coords[:,1,:]

        comm = MPI.COMM_WORLD
        nprocs = comm.Get_size()
        myrank = comm.Get_rank()

        group_size = 2000
        write_log("--"*12+"\n"+"** NB PROCESS: "+str(nprocs)+"\n", mode="a")
        group_chunks = [all_trips[i:i+group_size] for i in range(0, len(all_trips), group_size)]
        group_chunks = group_chunks[:nprocs]

        my_input = comm.scatter(group_chunks, root=0)

        # Process the input for each rank
        result = DelftBlue_getsPts(my_input, data, start, end,  full_edgesDf, edgesDf, myrank)

        # Gather results from all processes
        results = comm.gather(result, root=0)
    
    if function_name=="DelftBlue_matchTrace":
        outputdir = sys.argv[2] 
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        if not os.path.exists(outputdir+'viterbi/'):
            os.mkdir(outputdir+'viterbi/')
        h5_folder = "/".join(outputdir.split("/")[:-2])+"/ptsDf_/"
        
        map_graph = "raw_map/graph.graphml"
        coarse_street_map = "mapMatch_result/penalized_coarse_roads.csv"

        map_graph=nx.read_graphml(map_graph, node_type=int)
        edgesDf = pd.read_csv(coarse_street_map)
        edgesDf.set_index("edge", inplace=True)
   
        penality_dict={}
        beta_list = [alpha0, alpha1, alpha2, alpha3]
        class_list = [class0, class1, class2, class3]
        for class_, beta_ in zip(class_list, beta_list):
            for i in class_:
                penality_dict[i]=beta_
        for i in class4:
            penality_dict[i]=0  
            
        edge_dict = {str((ii,jj)):kk for ii, jj, kk in zip(edgesDf.source,edgesDf.target, edgesDf.index)}
        
        uturncost = (edgesDf.cost.median()+edgesDf.reverse_cost.median())/2.

        comm = MPI.COMM_WORLD
        nprocs = comm.Get_size()
        myrank = comm.Get_rank()
        
        result = DelftBlue_matchTrace(map_graph,edgesDf,h5_folder, uturncost, myrank)
        # Gather results from all processes
        results = comm.gather(result, root=0)
