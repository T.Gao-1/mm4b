import os
import time

import glob
import datetime
import networkx as nx

import geopandas as gpd
import pandas as pd
import numpy as np
import scipy.sparse as sp
from scipy.sparse import csr_matrix

from utils import *
from scipy import stats, sparse
from geopy.distance import geodesic
from shapely.geometry import LineString

try:
    from config_mapmatch import *
except ModuleNotFoundError:
    raise Warning('config file for map matchingnot found')

def graph_construct(df):
    oneway = df.drop(columns=["reverse_cost"]).copy()
    reverse = df.drop(columns=["cost"]).copy().rename(columns={"reverse_cost":"cost", "target":"source", "source":"target"})
    twoway = pd.concat([oneway, reverse]).sort_values(by="cost")
    twoway = twoway.drop_duplicates(subset=["source", "target"], keep="first")
    twoway = twoway.sort_index()
    df = twoway.copy()

    G=nx.DiGraph()
    for idx, row in df.iterrows():
        source = row['source']
        target = row['target']
        cost = row['cost']
        km = row["km"]
        type_ = row["type"]
        G.add_edge(source, target, cost=cost, distance=km,  roadtype=type_, index=idx)
    
    nx.write_graphml(G, map_nx_path)
    return G


def complete_twoway_edges(edgesDf):
    '''
    Objective: for oneway==0 (bidirectional roads), add both direction explicitly in the dataframe
    '''
    edgesDf["edge"] = edgesDf.index
    twoway_ = edgesDf[edgesDf["oneway"]==0]
    arr1, arr2 = twoway_.source.to_numpy(), twoway_.target.to_numpy()
    twoway = twoway_.copy()
    twoway['source'], twoway['target'] = arr2, arr1
    df = pd.concat([twoway, edgesDf]).reset_index(drop=True)
    # Add 10% to the distance of footway and residential roads
    df.loc[df["type"].isin([i for i in bikable_type if i!="cycleway"]), "km"] = df.loc[df["type"].isin(["footway", "residential"]), "km"]*1.1
    return df

def build_graph(input_df):    
    '''
    strict_G: directed complete road network graph
    bikeable_G: directed road network graph with only bikeable roads
    full_G: undirected complete road network graph 
    '''                    
    strict_G, bikable_G, full_G = nx.DiGraph(), nx.DiGraph(), nx.DiGraph()
    for idx, row in input_df.iterrows():
        source = row['source']
        target = row['target']
        cost = row['cost']
        km = row["km"]
        type_ = row["type"]
        strict_G.add_edge(source, target, cost=cost, distance=km, roadtype=type_, label=idx)
        full_G.add_edge(source, target, cost=cost, distance=km, roadtype=type_, label=idx)               
        if row["oneway"]==1:
            full_G.add_edge(target, source, cost=cost, distance=km, roadtype=type_, label=idx)
        if type_ in bikable_type:
            bikable_G.add_edge(source, target, cost=cost, distance=km, roadtype=type_, label=idx)
    return strict_G, bikable_G, full_G

def enlarge_downstream_scope(current_node, graph, depth, max_km, raw_km, loop_tolerance_coeff,visited, 
                    inverse_km, ver_km, 
                    browsed_km, new_added_km, new_edge_idx,
                    browsed_km_dict, browsed_trajectory_dict):
    '''
    This function records the distance and path for each downstream node of the current node
    Special case (a) is considered
    browsed_trajectory_dict: dict[e_4] = [e_1, e_1, e_2, e_3], shows the path from the initial starting node to the current node
    browsed_km_dict: dict[e_4] = 0.1, shows the distance from the initial starting node to the current node
    '''
    if browsed_km>=max_km or depth>=max_depth:
        return browsed_km_dict, browsed_trajectory_dict
    
    # Update the browsed_trajectory_dict
    browsed_trajectory_dict[current_node]=browsed_trajectory_dict[visited[-1]]+[new_edge_idx]

    if len(visited)>=2: 
        # Special Case (a), cacluate the vertical and inverse distance between the original start node and the current node
        original_start_node = str2lst(idpunt_dict[visited[0]])
        original_end_node = str2lst(idpunt_dict[visited[1]])
        new_node = str2lst(idpunt_dict[current_node])
        last_added_node = str2lst(idpunt_dict[visited[-1]])
        vec1, vec2 = original_start_node-original_end_node, new_node-last_added_node
        angle = np.arccos(np.sum(vec1*vec2)/np.sqrt(np.sum(vec1*vec1)*np.sum(vec2*vec2)))
        ver_km += (np.sin(angle)*new_added_km)
        inverse_km += np.maximum((np.cos(angle)*new_added_km), 0)
        if (inverse_km<=1e-10 and ver_km>=0.1):
            pass
        elif (ver_km>=min(loop_tolerance_coeff*(browsed_km+raw_km), 0.04) or inverse_km>=min(loop_tolerance_coeff*(browsed_km+raw_km), 0.04)):
            return browsed_km_dict, browsed_trajectory_dict
        
    # Find the incoming and outgoing bikeable edges of the current node, as well as all the outgoing unbikeable edges
    outgoing_edges = [i for i in graph.out_edges(current_node, data=True) if i[1] not in visited and i[2].get('roadtype') in unbikeable_type]
    out_bikeable = [i for i in graph.out_edges(current_node, data=True) if i[2].get('roadtype') in bikable_type]
    in_bikeable = [i for i in graph.in_edges(current_node, data=True) if i[2].get('roadtype') in bikable_type]
    
    # Update the browsed_km_dict with browsed_km if the current node if connected to the bikeable roads
    if len(out_bikeable) or len(in_bikeable)>0:
        browsed_km_dict[current_node]=browsed_km   
    # If the current node only have no outgoing roads, put the end to it
    if len(out_bikeable)+len(outgoing_edges)==0:
        browsed_km_dict[current_node]=-1

    # Recursively search the downstream nodes
    for next_edge in outgoing_edges:
        browsed_km_dict, browsed_trajectory_dict = enlarge_downstream_scope(next_edge[1],graph=graph, depth=depth+1, max_km=max_km, raw_km=raw_km, loop_tolerance_coeff=loop_tolerance_coeff,visited=visited+[current_node], 
                                                                      ver_km=ver_km, inverse_km=inverse_km,
                                                                      browsed_km=browsed_km+next_edge[2]["distance"], new_added_km=next_edge[2]["distance"],new_edge_idx=next_edge[2]["label"],
                                                                      browsed_km_dict=browsed_km_dict, browsed_trajectory_dict=browsed_trajectory_dict, 
                                                )
    return browsed_km_dict, browsed_trajectory_dict

def enlarge_upstream_scope(current_node, graph, depth, max_km, raw_km, loop_tolerance_coeff,visited, 
                    inverse_km, ver_km, 
                    browsed_km, new_added_km, new_edge_idx,
                    browsed_km_dict, browsed_trajectory_dict):

    if browsed_km>=max_km or depth>=max_depth:
        return browsed_km_dict, browsed_trajectory_dict
        
    browsed_trajectory_dict[current_node]=browsed_trajectory_dict[visited[-1]]+[new_edge_idx]
    
    if len(visited)>=2:# and raw_km<0.1:
        original_start_node = str2lst(idpunt_dict[visited[0]])
        original_end_node = str2lst(idpunt_dict[visited[1]])
        new_node = str2lst(idpunt_dict[current_node])
        last_added_node = str2lst(idpunt_dict[visited[-1]])
        vec1, vec2 = original_start_node-original_end_node, new_node-last_added_node
        angle = np.arccos(np.sum(vec1*vec2)/np.sqrt(np.sum(vec1*vec1)*np.sum(vec2*vec2)))            
        ver_km += (np.sin(angle)*new_added_km)
        inverse_km += np.maximum((np.cos(angle)*new_added_km), 0)
        
        if (inverse_km<=1e-10 and ver_km>=0.1):
            pass
        elif (ver_km>=min(loop_tolerance_coeff*(browsed_km+raw_km), 0.04) or inverse_km>=min(loop_tolerance_coeff*(browsed_km+raw_km), 0.04)):# or (ver_km+inverse_km)/(km+raw_km)>=0.3):#if (ver_km/(km+raw_km)>=0.2 or inverse_km/(km+raw_km)>=0.2  or (ver_km+inverse_km)/(km+raw_km)>=0.3):# or ver_km>=0.02 or inverse_km>=0.02):
            return browsed_km_dict, browsed_trajectory_dict
    
    incoming_edges = [i for i in graph.in_edges(current_node, data=True) if i[0] not in visited and i[2].get('roadtype') in unbikeable_type]
    # print(incoming_edges)
    out_bikeable = [i for i in graph.out_edges(current_node, data=True) if i[2].get('roadtype') in bikable_type]
    in_bikeable = [i for i in graph.in_edges(current_node, data=True) if i[2].get('roadtype') in bikable_type]
    if len(out_bikeable) or len(in_bikeable)>0: 
        browsed_km_dict[current_node]=browsed_km   
    if len(incoming_edges)+len(in_bikeable)==0:
        browsed_km_dict[current_node]=-1

    for next_edge in incoming_edges:
        browsed_km_dict, browsed_trajectory_dict = enlarge_upstream_scope(next_edge[0], graph=graph, depth=depth+1, max_km=max_km, raw_km=raw_km, loop_tolerance_coeff=loop_tolerance_coeff,visited=visited+[current_node], 
                                                                      ver_km=ver_km, inverse_km=inverse_km,
                                                                      browsed_km=browsed_km+next_edge[2]["distance"], new_added_km=next_edge[2]["distance"],  new_edge_idx=next_edge[2]["label"],
                                                                    browsed_km_dict=browsed_km_dict, browsed_trajectory_dict=browsed_trajectory_dict)
    return browsed_km_dict, browsed_trajectory_dict

def clean_candidate(bikable_G, full_G, expanded_nodes):
    '''
    This function considers special case (b), ensuring the connectivity of the bikeable roads
    '''
    # We only need to consider nodes that connects to the bikeable roads
    candidates = [i for i in expanded_nodes if bikable_G.has_node(i)]
    bad_expanded_nodes = []
    for i in candidates:
        for j in candidates:
            if i==j:
                continue
            # Calculate the shortest path distance for the full road network
            shortest_path_fullG=nx.shortest_path(full_G, source=i, target=j, weight='distance', method='dijkstra')
            shortest_dist_fullG = sum(full_G[u][v]["distance"] for u, v in zip(shortest_path_fullG[:-1], shortest_path_fullG[1:]))
            try:
                shortest_path_bikeableG=nx.shortest_path(bikable_G, source=i, target=j, weight='distance', method='dijkstra')
            except:
                # If the bikeable road network is not connected, we need to remove the corresponding road
                if j in full_G[i].keys():
                    bad_expanded_nodes.append(full_G[i][j]["label"])
                continue
            shortest_dist_bikeableG = sum(bikable_G[u][v]["distance"] for u, v in zip(shortest_path_bikeableG[:-1], shortest_path_bikeableG[1:]))
            # If it causes significant detour to the bikeable road network
            if shortest_dist_bikeableG/shortest_dist_fullG>=network_interrupt_threshold: 
                bad_expanded_nodes+=[full_G[source_node][target_node]['label'] for source_node, target_node in zip(shortest_path_fullG[:-1], shortest_path_fullG[1:])]
                break   
    return bad_expanded_nodes   
   
def access_candidate_roads(bikable_G, upstream_browsed_km, downstream_browsed_km, raw_km, upstream_browsed_path, downstream_browsed_path,upstream_bad_expanded_nodes, downstream_bad_expanded_nodes):
    '''
    raw_km: the length of our considered unconfirmed car road
    This function accesses if the found alternative roads are ligible
    '''
    all_possibilities = []
    
    for upstream_node in upstream_browsed_km.keys():
        km1 = upstream_browsed_km[upstream_node]
        for downstream_node in downstream_browsed_km.keys():
            km2 = downstream_browsed_km[downstream_node]

            if upstream_node==downstream_node:
                continue
            try:
                shortest_path_list=nx.shortest_path(bikable_G, source=upstream_node, target=downstream_node, weight='distance', method='dijkstra')
            except:
                # If the bikeable road network is not connected
                # We could still keep the expanded upstream and downstream nodes
                # If they are at the frontier of the network besides those listed in frontier_exception_nodes
                if km1==-1 or km2==-1 and downstream_node not in frontier_exception_nodes:
                    all_possibilities += upstream_browsed_path[upstream_node]
                    all_possibilities += downstream_browsed_path[downstream_node]
                continue

            bike_km = sum([bikable_G[shortest_path_list[i]][shortest_path_list[i + 1]]['distance'] for i in range(0,len(shortest_path_list)-1)])
            unbikeable_km = raw_km+km1+km2

            if bike_km/unbikeable_km<=case1_theta or (abs(unbikeable_km-bike_km)<=case3_delta and bike_km/unbikeable_km<=case2_theta):
                for i in upstream_browsed_path[upstream_node]:
                    if i in upstream_bad_expanded_nodes:
                        break
                    all_possibilities.append(i)
                for i in downstream_browsed_path[downstream_node]:
                    if i in downstream_bad_expanded_nodes:
                        break
                    all_possibilities.append(i)
    return all_possibilities
    
def access_by_row(idx, row, strict_G, bikable_G, full_G):
    '''
    This function aims to find the alternative roads for the unconfirmed roads
    '''
    source_node, target_node = row["target"], row["source"]
    loop_tolerance_coeff = ver_invser_km_dict[row["type"]]
    downstream_browsed_km, downstream_browsed_path = enlarge_downstream_scope(current_node=source_node, graph=strict_G, depth=0, max_km=row["km"]*max_km_coefficient, raw_km=row["km"], loop_tolerance_coeff=loop_tolerance_coeff,visited=[target_node],
                                                  inverse_km=0, ver_km=0,  
                                                  browsed_km=0, new_added_km=0, new_edge_idx=idx,
                                                  browsed_km_dict={}, browsed_trajectory_dict={target_node:[]} )
    downstream_bad_expanded_nodes = clean_candidate(bikable_G, full_G, downstream_browsed_km.keys())
    if len(downstream_browsed_km) == 0:
        return [],[]
    upstream_browsed_km, upstream_browsed_path = enlarge_upstream_scope(current_node=target_node, graph=strict_G, depth=0, max_km=row["km"]*max_km_coefficient, raw_km=row["km"], loop_tolerance_coeff=loop_tolerance_coeff,visited=[source_node],
                                                              inverse_km=0, ver_km=0, 
                                                              browsed_km=0, new_added_km=0, new_edge_idx=idx,
                                                            browsed_km_dict={}, browsed_trajectory_dict={source_node:[]} )
    upstream_bad_expanded_nodes = clean_candidate(bikable_G, full_G, upstream_browsed_km.keys())
    if len(upstream_browsed_km)==0:  
        return [],[]
    new_confirmed = access_candidate_roads(bikable_G=bikable_G, upstream_browsed_km=upstream_browsed_km, downstream_browsed_km=downstream_browsed_km, 
                                                                  raw_km=row["km"], 
                                                                  upstream_browsed_path=upstream_browsed_path, downstream_browsed_path=downstream_browsed_path,
                                                                  upstream_bad_expanded_nodes=upstream_bad_expanded_nodes, downstream_bad_expanded_nodes=downstream_bad_expanded_nodes)
    return new_confirmed
    
def evaluate_unconfirmed_main_car_roads():
    edgesDf = pd.read_csv(simplified_graph_path)
    edgesDf.set_index("edge", inplace=True)
    df = complete_twoway_edges(edgesDf)
    unconfirmed_roads_df = df[df["type"].isin(unconfirmed_roads_type)]

    strict_G, bikable_G, full_G = build_graph(df)

    confirmed_car_roads_idx_lst = []
    for edge in unconfirmed_roads_df["edge"].unique():
        bidirection_expanded_roads = []
        # For each edge, it could have two rows corresponding to two directions
        # Only when these two directions both have alternative bikeable roads, we can assign it with low availability
        for idx, row in unconfirmed_roads_df[unconfirmed_roads_df["edge"]==edge].iterrows():
            candidate = access_by_row(idx, row, strict_G, bikable_G, full_G)
            if idx in candidate:
                bidirection_expanded_roads.append(candidate)
        if len(unconfirmed_roads_df[unconfirmed_roads_df["edge"]==edge])==len(bidirection_expanded_roads):
            confirmed_car_roads_idx_lst+=[i for j in bidirection_expanded_roads for i in j]

    good_edges = df.drop(confirmed_car_roads_idx_lst)["edge"].unique()
    confirmaed_car_roads_lst = list(set(df["edge"].unique())-set(good_edges))
    edgesDf.loc[confirmaed_car_roads_lst, "penality"] = 1
    edgesDf.loc[edgesDf["type"].isin(confirmed_car_roads_type), "penality"] = 1
    all_confirmed_car_roads = ",".join([str(i) for i in edgesDf[edgesDf["penality"]==1].index.to_list()])

    os.system("python visual_map_matching.py --function_name=penality_roads_plot --input_file="+all_confirmed_car_roads+" --output_path="+confirmed_roads_html)
    print("The confirmed car roads are visualized in "+confirmed_roads_html+".\nPlease input the roads that you want to keep, separated with \",\", q to exit.")
    input_str = input()
    while input_str != "q":
        wrongly_selected_ones = input_str.split(",")
        all_confirmed_car_roads = ",".join([i for i in all_confirmed_car_roads.split(",") if i not in wrongly_selected_ones])
        os.system("python visual_map_matching.py --function_name=penality_roads_plot --input_file="+all_confirmed_car_roads+" --output_path="+confirmed_roads_html)
        print("The visualization is updated, do you want to continue? q to exit.")
        input_str = input()    
    
    print("Do you want to manually add confirmed roads?, separated with \",\", q to exit.")
    input_str = input()
    while input_str != "q":
        all_confirmed_car_roads = all_confirmed_car_roads+","+input_str
        os.system("python visual_map_matching.py --function_name=penality_roads_plot --input_file="+all_confirmed_car_roads+" --output_path="+confirmed_roads_html)
        print("The visualization is updated, do you want to continue? q to exit.")
        input_str = input()           

    with open(confirmed_roads_txt, 'w') as file:
        file.write(all_confirmed_car_roads)
    
    all_confirmed_car_roads=[int(i) for i in all_confirmed_car_roads.split(",")]
    edgesDf["edge"]=edgesDf.index
    edgesDf["penality"]=0
    edgesDf.loc[all_confirmed_car_roads, "penality"]=1

    edgesDf["cost"]=edgesDf["km"]*1000
    beta_list = [beta0, beta1, beta2, beta3]
    class_list = [class0, class1, class2, class3]
    for class_, beta_ in zip(class_list, beta_list):
        edgesDf.loc[(edgesDf["type"].isin(class_))&(edgesDf["penality"]==0), "cost"] = edgesDf.loc[(edgesDf["type"].isin(class_))&(edgesDf["penality"]==0), "cost"]*(1+beta_)
    edgesDf.loc[edgesDf["penality"]==1, "cost"] = edgesDf.loc[edgesDf["penality"]==1, "cost"]*(1+beta4)

    edgesDf["reverse_cost"]=edgesDf["cost"]*(1+revser_coeffi*edgesDf["oneway"])
    edgesDf.to_csv(confirmed_coarse_path, index=False)
    edgesDf.set_index("edge", inplace=True)
    return edgesDf

if __name__ == "__main__":
    '''
    This file aims to evaluate the availability for roads in config_mapmatch/unconfirmed_roads_type
    Which is based on the existence of nearby alternative roads of config_mapmatch/bikable_type
    The road network file is stored in config_mapmatch/confirmed_coarse_path
    The visualtion is available in config_mapmatch/confirmed_roads_html
    '''
    _, idpunt_dict, _, _ = get_map_dict(None, point_save_path)
    edgesDf = evaluate_unconfirmed_main_car_roads()
    graph_construct(edgesDf)
